<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'TQYNLavHwPSN6D1C9mDqXXfmboWP88dQ/zWWIFHQm2JEn3QkQVUE761sEJDfqP5aM2qC4IZZLa+4eeB23HvvXQ==');
define('SECURE_AUTH_KEY',  '0M4/D9k95m+bmlbjpHoNjliSIicMrj9WWs1gsryLjUmnz/mP9CCkBrqKDuh9flst/BDtHNBDTVt7pDxCBYkJww==');
define('LOGGED_IN_KEY',    'CBjnBo/lUO8xWzMeOuBo4HEVzNtsuAL82H6b3SbjB+PmsdW3nKko3o3ruyiLBhCpoVyHY5soZe/QTVDnNqmqwQ==');
define('NONCE_KEY',        'gOGqCptVq8uKMTARjpCn6gUyF8IAUzIw8LRLUUTC1Lsv5ilLmAkIKyqBtOjQvzDGiRA7ft+reMM0AgTOvhO1RQ==');
define('AUTH_SALT',        '1BnyT6C78kExxvpxR+gIe8Nz4MD1FFuTyC3bgVk5ZSFFZc9hmmC+9Bu6cTMXtSjbqvTXOjUpzwYVMkODsdGqow==');
define('SECURE_AUTH_SALT', '2zvgaul+6nbsuokJP/5drLDaMhT2aW+pn9KwhiFPA7H0RP1s2K1vRJmCyUs+1iLfmMPs/kz+pOOzfNNNaK5lJA==');
define('LOGGED_IN_SALT',   'X5hUfPPGZXbbfUCwimsoa2GOlo9ulq+u2wN/SzDc9plwlmwxmIBsnRMXsCQ8bBHnV6m2zRl69TTfat8ltJXMcg==');
define('NONCE_SALT',       'Y8qOs5RsokbCrGwYbm6s6xU2h4d8KCupB9cnjEFCSJIvT3tI/gDjX/7Tz48311Usmbav2oFEaJk2eVzVoRtFdA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
