<?php
function projeto_scripts() {
wp_enqueue_style( "reset-sheet",  get_stylesheet_directory_uri() . "/reset.css");
wp_enqueue_style( "grid-sheet",  get_stylesheet_directory_uri() . "/grid.css");
wp_enqueue_style( "style-sheet",  get_stylesheet_directory_uri() . "/style.css");

}
add_action( 'wp_enqueue_scripts', 'projeto_scripts' );

add_theme_support("menus");

//function register_my_menu(){
  //  register_nav_menu('navegacao', __('navegação'));
//}

//add_action( 'init', 'register_my_menu' );


function custom_post_type(){ //Criando post Customizado
	register_post_type(
		'noticia',
		array(
			'label' => 'Notícias',
			'description' => 'Notícias',
			'menu_position' => 2,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post' ,
			'map_meta_cap' => true,
			'hierarchical' => false,
			'query_var' => true,
			'supports'  => array('title', 'editor'),

			'labels' => array(
				'name' => 'Notícia',
				'singular_name' => 'Notícia',
				'menu_name' => 'Notícias',
				'add_new' => 'Nova Notícia',
				'add_new_item' => 'Adicionar Nova Notícia',
				'edit' => 'Editar Noticia',
				'edit_item' => 'Editar Notícia',
				'new_item' => 'Nova Notícia',
				'view' => 'Ver Notícia',
				'view_item' => 'Ver Noticia',
				'search_items' => 'Procurar Noticia',
				'not_found' => 'Nenhuma Noticia Encontrada',
				'not_found_in_trash' => 'Nenhuma Notícia Encontrada no lixo'
			)

		)
	);
}

add_action('init','custom_post_type');



?>