<?php
// Template Name: Home Page

?>



<?php get_header(); ?>

<main class="inicio">
		<h1><?php the_field('titulo_chamativo') ?></h1>
		<p><?php the_field('subtitulo_chamativo') ?></p>
	</main>
	
	<section class="sobre" id="sobre">

		<p><?php the_field('bio') ?></p>
	</section>
	
	
	
	<section class="unidades" id="unidades">
		<div class="unidades-item container">
			<div class="grid6">
				<img src="<?php the_field('inlogo') ?>" >
			</div>
			<div class="grid6">
				<h2><?php the_field('info_local') ?></h2>
			</div>
		</div>
		
		<div class="unidades-item container">
			<div class="grid6">
				<img src="<?php the_field('inlogo') ?>" >
			</div>
			<div class="grid6">
				<h2><?php the_field('info_local') ?></h2>

			</div>
		</div>
		
		<div class="unidades-item container">
			<div class="grid6">
				<img src="<?php the_field('inlogo') ?>" >
			</div>
			<div class="grid6">
				<h2><?php the_field('info_local') ?></h2>
			</div>
		</div>

		<div class="unidades-item container">
			<div class="grid6">
				<img src="<?php the_field('inlogo') ?>" >
			</div>
			<div class="grid6">
				<h2><?php the_field('info_local') ?></h2>
			</div>
		</div>
	
	</section>

	<section>
		
	</section>
	

	
	<?php get_footer(); ?>