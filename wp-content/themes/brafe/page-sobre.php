<?php
// Template Name: Sobre

?>

<?php get_header(); ?>
<h1>Sobre</h1>	

<div class="unidades-item container">
			<div class="grid6">
				<img src="<?php the_field('sobreimagem') ?>" >

</div>

            
<section class="sobre" id="sobre">

		<p><?php the_field('sobretexto') ?></p>
</section>
	
<section class = redes>
	<a id = "link" href = "https://www.facebook.com/injunioruff/"><img src="<?php the_field('facebook_logo') ?>"></a>
	<a id = "link" href = "https://www.instagram.com/injunioruff/"><img src="<?php the_field('instagram_logo') ?>"></a>

</section>

	
<?php get_footer(); ?>